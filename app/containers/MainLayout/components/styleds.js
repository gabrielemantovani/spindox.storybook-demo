/**
 * Created by Stefano Demurtas on 25/10/2017.
 */

import styled from 'styled-components';
import getModalStyle from 'components/Organisms/Modal/styles';

export const MainLayoutWrapper = styled.div`
  position: relative;
  min-height: 100vh;
  height: 100%;
  width: 100vw;
  padding-top: ${(props) => props.theme.sizes.headerHeight};
  background-color: ${(props) => props.theme.colors.background};
  
  ${(props) => getModalStyle(props)};
`;

