// Mixins function

import { keyframes } from 'styled-components';

export const truncate = () => `
  display: block;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

export const truncateLines = (n) => `
  display: -webkit-box;
  text-overflow: ellipsis;
  overflow: hidden;
  -webkit-line-clamp: ${n};
  -webkit-box-orient: vertical;
`;

/* eslint-disable no-tabs */
export const bounceUp = keyframes`
  0%, 100% {
    transform: translate3d(0, 0, 0);
   }
	50% {
    transform: translate3d(0, -10px, 0);
  }
`;

export const placeholderPulse = keyframes`
  0% {
    opacity: .6;
  }
  50% {
    opacity: 1;
  }
  100% {
    opacity: .6;
  }
`;

export const loaderSpin = keyframes`
  0%{
    -webkit-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }

  100%{
    -webkit-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
`;
/* eslint-enable */
