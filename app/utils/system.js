
import isEmail from 'validator/lib/isEmail';
import isURL from 'validator/lib/isURL';

const openPhone = (number) => window.open(`tel:${number.replace(/\s/g, '')}`, 'target=_system');

const openMail = (mail) => {
  if (isEmail(mail)) window.open(`mailto:${mail}`, 'target=_system');
};

const openExternalLink = (link) => {
  if (isURL(link)) window.open(link, 'target=_system');
};

export { openPhone, openMail, openExternalLink };
