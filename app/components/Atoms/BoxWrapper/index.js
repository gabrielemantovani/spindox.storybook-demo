/**
*
* BoxWrapper
*
*/

import styled from 'styled-components';

const BoxWrapper = styled.div`
  display: block;
  overflow: hidden;
  width: 100%;
  padding: 0 ${(props) => props.theme.sizes.paddingFrame};
  box-sizing: border-box;
`;

export default BoxWrapper;
