/**
 *
 * Loader
 *
 */

import styled from 'styled-components';

import { loaderSpin } from '../../../utils/mixins';

const Loader = styled.div`
  width: 45px;
  height: 45px;
  border-radius: 100%;
  position: relative;
  margin: 0 auto;
  
  &:before,
  &:after {
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    border-radius: 100%;
    border: 5px solid transparent;
    border-top-color: ${(props) => props.theme.colors.highlightBorder};
  }
  
  &:before {
    z-index: 100;
    animation: ${loaderSpin} 1s infinite;
  }
  
  &:after {
    border: 5px solid ${(props) => props.theme.colors.boxBackground};
  }
`;

export default Loader;
