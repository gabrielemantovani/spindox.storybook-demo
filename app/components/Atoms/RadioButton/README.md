# RadioButton

### Example
```js
import RadioButton from './components/RadioButton';

...
  return(
      <RadioButton
        value={this.state.value}
        onClick={() => this.setState(({ value }) => ({ value: !value }))}
        label="Yes"
      />
  );


...

```

### Required props:
  - **value:** State of radio (bool)
  - **onClick:** Handle func on click (func)
  - **label:** Label of radio button (string/node)
  - **disabled:** Flag to disable the button (button)
