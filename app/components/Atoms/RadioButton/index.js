/**
 *
 * RadioButton
 *
 */

import React from 'react';
import PropTypes from 'prop-types';

import {
  RadioButtonWrapper,
  RadioInput,
  RadioLabel,
  RadioInputWrapper,
} from './components/styleds';

class RadioButton extends React.PureComponent {
  // eslint-disable-line react/prefer-stateless-function
  render() {
    const { value, label, disabled, onClick, className } = this.props;
    const baseClass = 'radioButton';

    return (
      <RadioButtonWrapper className={`${baseClass} ${className}`}>
        <RadioInputWrapper value={value} onClick={!disabled && onClick}>
          <RadioInput value={value} disabled={disabled} />
        </RadioInputWrapper>
        {label && <RadioLabel value={value} disabled={disabled}>{label}</RadioLabel>}
      </RadioButtonWrapper>
    );
  }
}

RadioButton.propTypes = {
  value: PropTypes.bool,
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  className: PropTypes.string,
};

export default RadioButton;
