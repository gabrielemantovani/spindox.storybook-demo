import styled from 'styled-components';

export const RadioButtonWrapper = styled.div`
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
`;

export const RadioInputWrapper = styled.div`
  width: 40px;
  height: 40px;
  border-radius: 50%;
  border: solid 1px
    ${(props) =>
      props.value
        ? props.theme.colors.radioActiveBorder
        : props.theme.colors.radioBorder};
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const RadioInput = styled.div`
  width: 18px;
  height: 18px;
  display: block;
  border-radius: 50%;
  border: solid 1px ${(props) => {
    if (props.value) {
      return props.theme.colors.radioSelected;
    } else if (props.disabled && !props.value) {
      return props.theme.colors.radioBorder;
    }
    return props.theme.colors.radioSelected;
  }};
  background-color: ${(props) =>
    props.value ? props.theme.colors.radioSelected : 'transparent'};
`;

export const RadioLabel = styled.div`
  display: block;
  text-align: center;
  padding-top: 12px;
  flex: 1 1 100%;
  font-family: ${(props) => props.theme.fonts.primaryFont};
  font-size: ${(props) => props.theme.sizes.smallText};
  color: ${(props) => props.value ? props.theme.colors.primaryText : props.theme.colors.secondaryText};
`;
