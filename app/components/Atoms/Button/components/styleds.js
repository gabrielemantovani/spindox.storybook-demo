import styled from 'styled-components';

const ButtonBase = styled.button`
  min-width: 234px;
  width: ${(props) => (props.fixed || props.fullWidth) && '91vw'};
  text-align: center;
  padding: 0 8px;
  box-sizing: border-box;
  margin: 0 auto;
  transition: transform 0.3s;
  display: flex;
  justify-content: center;
  align-items: center;
  border: none;
  height: ${(props) => props.theme.sizes.buttonHeight};
  
  &:focus {
    outline: none;
  }

  &[disabled] {
    pointer-events: none;
  }

  i {
    padding-right: 12px;
  }

  .button__label {
    text-align: center;
    font-family: ${(props) => props.theme.fonts.primaryFont};
    font-size: ${(props) => props.theme.sizes.text};
    text-transform: uppercase;
  }
`;

const getPosition = (props) => {
  if (props.fixed) {
    return `
      position: absolute;
      bottom: 20px;
      left: 0;
      right: 0;
      transform: ${props.visible ? 'translateY(0)' : 'translateY(100px)'};
      z-index: ${props.theme.constants.layers.indexOf('ButtonFixed')};
    `;
  }
  return `
      position: relative;
    `;
};

const getStyle = (props) => {
  const styles = {
    primary: `
      background-color: ${
        props.disabled
          ? props.theme.colors.backgroundButtonDisabled
          : props.theme.colors.backgroundButton
      };
      color: ${props.theme.colors.primaryText};
    `,
    secondary: `
      background-color: transparent;
      display: flex;
      flex-wrap: wrap;
      color: ${
        props.disabled
          ? props.theme.colors.secondaryText
          : props.theme.colors.primaryText
      };
      border: solid 0.5px ${
        props.disabled
          ? props.theme.colors.backgroundButtonDisabled
          : props.theme.colors.highlightBorder
      };
      border-radius: 2px;
      
      .icon {
        padding-bottom: 32px;
        padding-right: 0;
        color: ${props.theme.colors.highlightText};
      }

        
      ${props.icon && `
        padding: 40px 8px; 
        min-width: auto;
        width: 100%;
        min-height: 160px;
        height: auto;
        
        .button__label {
          display: block;
          width: 100%;
          flex-basis: 100%;
        }
      `}
    `,
    primaryGhost: `
      background-color: transparent;
      color: ${props.theme.colors.highlightText};
      
      i {
        color: ${props.theme.colors.highlightText};
      }
    `,
    secondaryGhost: `
      background-color: transparent;
      color: ${props.theme.colors.primaryText};  
      
      i {
        color: ${props.theme.colors.primaryText}; 
      } 
    `,
    link: `
      width: auto;
      min-width: auto;
      background-color: transparent;
      color: ${props.theme.colors.highlightText};
      
      .button__label {
        text-transform: capitalize;
      }
    `,
  };

  return styles[props.type];
};

export const ButtonWrapper = styled(ButtonBase)`
  ${(props) => getStyle(props)};
  ${(props) => getPosition(props)};
`;
