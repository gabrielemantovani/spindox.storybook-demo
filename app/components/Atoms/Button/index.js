/**
 *
 * ButtonCta
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import tmripple from 'touchmyripple';

// components
import Icon from 'components/Atoms/Icon';

import { ButtonWrapper } from './components/styleds';

class Button extends React.PureComponent {
  // eslint-disable-line react/prefer-stateless-function

  componentDidMount() {
    if (this.props.disableRipple) return;
    tmripple.init();
  }

  render() {
    const {
      onClick,
      label,
      icon,
      fixed,
      visible,
      disabled,
      style,
      disableRipple,
      className,
      ...rest
    } = this.props;
    const baseClass = 'button';

    return (
      <ButtonWrapper
        {...{ fixed, disabled, visible, style, onClick, icon }}
        data-animation={!disableRipple && 'ripple'}
        className={`${baseClass} ${className}`}
        {...rest}
      >
        {icon && <Icon name={icon} />}
        <span className={`${baseClass}__label`}>{label}</span>
      </ButtonWrapper>
    );
  }
}

Button.defaultProps = {
  type: 'primary',
  className: '',
};

Button.propTypes = {
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  icon: PropTypes.string,
  fixed: PropTypes.bool,
  visible: PropTypes.bool,
  style: PropTypes.object,
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
  type: PropTypes.string.isRequired,
  fullWidth: PropTypes.bool,
  disableRipple: PropTypes.bool,
  className: PropTypes.string,
};

export default Button;
