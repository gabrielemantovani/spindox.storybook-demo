# Button

### Example
```js
import Button from './components/Button';

...
  return(
    <Button label="ADD NEW CAR" type="primary" />
  )


...

```

### Required props:
  - **type** type of button, default: 'primary' (string) - `['primary', 'secondary', 'primaryGhost', 'secondaryGhost']`
  - **label:** Content label (string/node)
  - **icon:** Icon inside the button (string/node)
  - **fixed:** Bottom fixed position with full width (bool)
  - **disabled:** Disabled state (bool)
  - **style:** Style object to customize css (object)
  - **onClick** Handle click (func)
