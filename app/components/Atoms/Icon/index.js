/**
 *
 * Icon
 *
 */

import React from 'react';
import PropTypes from 'prop-types';

// styled
import { IconElement } from './components/styleds';

class Icon extends React.PureComponent {
  // eslint-disable-line react/prefer-stateless-function
  render() {
    const { name, size, color, style, className, ...rest } = this.props;
    return (
      <IconElement
        {...{ size, color, style, ...rest }}
        className={`icon icon-${name} ${className}`}
      />
    );
  }
}

Icon.defaultProps = {
  className: '',
};

Icon.propTypes = {
  name: PropTypes.string.isRequired,
  size: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  color: PropTypes.string,
  style: PropTypes.object,
  className: PropTypes.string,
};

export default Icon;
