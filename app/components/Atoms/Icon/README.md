# Icon

### Example
```js
import Icon from './components/Icon';

...
  return(
    <Icon name="star-on" />
  );


...

```

### Required props:
  - **name:** Icon name (string)
  - **size:** Icon size (number in rem)
  - **color:** Color of icon (string)
