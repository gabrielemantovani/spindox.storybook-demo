import styled from 'styled-components';

const getSize = ({ size, ...props }) => {
  if (size) {
    return typeof size === 'number' && `${size}rem`;
  }
  return props.theme.sizes.icon;
};

export const IconElement = styled.i`
  font-size: ${(props) => getSize(props)};
  color: ${({ color, ...props }) => (color || props.theme.colors.icon)};
`;
