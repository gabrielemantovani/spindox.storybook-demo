/*
 * TextInput Messages
 *
 * This contains all the text for the GarageWidget component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  defaultErrorLabel: {
    id: 'app.containers.TextInput.defaultErrorLabel',
    defaultMessage: 'not valid',
  },
});
