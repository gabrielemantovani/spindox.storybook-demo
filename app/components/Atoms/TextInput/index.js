/**
 *
 * TextInput
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import isNil from 'lodash/isNil';
import isEmpty from 'lodash/isEmpty';
import toString from 'lodash/toString';


import {
  TextInputWrapper,
  InputField,
  InputLabel,
  InputError,
  InputOptionalLabel,
  ClickThroughField,
} from './components/styleds';

import messages from './messages';

const isVoid = (value) => isNil(value) || isEmpty(toString(value));

const defaultErrorLabel = <FormattedMessage {...messages.defaultErrorLabel} />;

class TextInput extends React.PureComponent {
  // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);

    this.state = {
      focused: props.focused,
      empty: isVoid(props.value),
      hasError: false,
    };

    this.handleFocus = this.handleFocus.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
  }

  componentDidMount() {
    this.checkDirty();
  }

  componentWillReceiveProps(nextProps) {
    const { value, focused } = nextProps;
    this.setState({
      empty: isVoid(value),
      focused,
    });
  }

  checkDirty() {
    const { value } = this.props;

    this.setState({
      empty: isVoid(value),
    });
  }

  handleFocus() {
    this.setState({
      focused: true,
    });
  }

  handleBlur({ target }) {
    const { validate } = this.props;
    this.setState({
      focused: false,
      hasError: validate ? !validate(target.value) : false,
    });
    this.checkDirty();
  }

  render() {
    const {
      id,
      type,
      label,
      errorLabel,
      disabled,
      value,
      className,
      clickThrough,
      optional,
      ...rest
    } = this.props;
    const { focused, empty, hasError } = this.state;
    const baseClass = 'textInput';

    return (
      <TextInputWrapper
        onFocus={this.handleFocus}
        onBlur={this.handleBlur}
        className={`${baseClass} ${className}`}
      >
        {
          clickThrough ?
            <ClickThroughField
              className={`${baseClass}__input`}
              {...{ id, type, disabled, hasError }}
              {...rest}
            >{ value }</ClickThroughField>
            :
            <InputField
              className={`${baseClass}__input`}
              {...{ id, type, value, disabled, hasError }}
              {...rest}
            />
        }
        <InputLabel
          className={`${baseClass}__label`}
          for={id}
          floated={focused || !empty}
          disabled={disabled}
        >
          {label}
          {optional && <InputOptionalLabel>
            (optional)
          </InputOptionalLabel>}
        </InputLabel>
        {hasError && <InputError>{errorLabel || defaultErrorLabel}</InputError>}
      </TextInputWrapper>
    );
  }
}

TextInput.defalutProps = {
  type: 'text',
  value: '',
  className: '',
};

TextInput.propTypes = {
  id: PropTypes.string.isRequired,
  type: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.string, PropTypes.number])),
  ]),
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  errorLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  validate: PropTypes.func,
  disabled: PropTypes.bool,
  focused: PropTypes.bool,
  clickThrough: PropTypes.bool,
  onChange: PropTypes.func,
  onClick: PropTypes.func,
  className: PropTypes.string,
  optional: PropTypes.bool,
};

export default TextInput;
