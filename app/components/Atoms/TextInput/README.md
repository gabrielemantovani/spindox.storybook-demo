# TextInput

### Example
```js
import TextInput from './components/TextInput';

...
  return(
    <TextInput 
      id="email" 
      label="Email" 
      value={this.state.email} 
      onChange={this.handleInputChange} 
      validate={(value) => isEmail(value)}
      />
  )


...

```

### Required props:
  - **id** unique id of input (string)
  - **type:** type of input (string)
  - **value:** value of input (string)
  - **label:** Label of input (string/node)
  - **errorLabel:** Label of error message (string/node)
  - **validate:** Function called onBlur to validate the input (func)
  - **disabled:** Boolean to trigger readonly (bool)
  - **onChange** Function call on input change (func)
