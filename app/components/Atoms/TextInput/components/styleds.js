import styled from 'styled-components';

const getInputLine = (props) => {
  if (props.disabled) return 'border: none';
  if (props.hasError) {
    return `border-bottom: solid 1px ${props.theme.colors.errorText}`;
  } else if (props.empty) {
    return `border-bottom: solid 1px ${props.theme.colors.inputLine}`;
  }
  return `border-bottom: solid 1px ${props.theme.colors.inputLineFilled}`;
};

export const TextInputWrapper = styled.div`
  display: flex;
  width: 100%;
  flex-direction: column;
  padding-bottom: 22px;
  position: relative;
  box-sizing: border-box;
`;

export const InputField = styled.input`
  -webkit-appearance: none;
  -webkit-tap-highlight-color: transparent;
  -webkit-font-smoothing: antialiased;
  -webkit-user-select: text;
  text-shadow: none;
  width: 100%;
  background: transparent;
  border: none;
  border-radius: 0;
  ${(props) => getInputLine(props)};
  -webkit-text-fill-color: ${(props) => props.theme.colors.primaryText};
  -webkit-opacity: 1;
  color: ${(props) => props.theme.colors.primaryText};
  font-size: ${(props) => props.theme.sizes.text};
  font-family: ${(props) => props.theme.fonts.primaryFont};
  padding-bottom: 8px;
  padding-left: 0;
  order: 2;

  &:focus {
    outline: none;
  }
`;

export const ClickThroughField = styled.div`
  -webkit-appearance: none;
  -webkit-tap-highlight-color: transparent;
  -webkit-font-smoothing: antialiased;
  -webkit-user-select: text;
  text-shadow: none;
  width: 100%;
  background: transparent;
  border: none;
  border-radius: 0;
  ${(props) => getInputLine(props)};
  -webkit-text-fill-color: ${(props) => props.theme.colors.primaryText};
  -webkit-opacity: 1;
  color: ${(props) => props.theme.colors.primaryText};
  font-size: ${(props) => props.theme.sizes.text};
  font-family: ${(props) => props.theme.fonts.primaryFont};
  padding-bottom: 8px;
  padding-left: 0;
  order: 2;

  &:focus {
    outline: none;
  }
`;

export const InputLabel = styled.label`
  display: block;
  -webkit-font-smoothing: antialiased;
  text-shadow: none;
  order: 1;
  pointer-events: none;
  color: ${(props) =>
    props.disabled || !props.floated
      ? props.theme.colors.inputLabel
      : props.theme.colors.highlightText};
  font-size: ${(props) => props.theme.sizes.text};
  font-family: ${(props) => props.theme.fonts.primaryFont};

  transition: all 0.2s ease;
  transform-origin: left top;
  transform: ${(props) =>
    props.floated
      ? 'scale(0.88) translate3d(0, 0, 0)'
      : 'scale(1) translate3d(0, 20px, 0)'};
`;

export const InputError = styled.div`
  display: block;
  position: absolute;
  bottom: 0;
  color: ${(props) => props.theme.colors.errorText};
  font-size: ${(props) => props.theme.sizes.smallText};
  font-family: ${(props) => props.theme.fonts.primaryFont};
`;

export const InputOptionalLabel = styled.span`
  display: inline-block;
  padding-left: 4px;
  text-transform: lowercase;
  color: ${(props) => props.theme.colors.secondaryText};
`;
