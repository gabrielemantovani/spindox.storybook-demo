/**
 *
 * TextInfo
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import _isArray from 'lodash/isArray';

// components
import Icon from 'components/Atoms/Icon';

// styled
import {
  TextInfoWrapper,
  Label,
  TextInfoDetail,
  TextInfoIcon,
  TextValue,
  WrapperValues,
} from './components/styleds';

class TextInfo extends React.PureComponent {
  // eslint-disable-line react/prefer-stateless-function

  static getValues(value) {
    if (_isArray(value)) {
      return value.map((e, i) => <TextValue key={i} onClick={e.onClick}>{e.text}</TextValue>); //eslint-disable-line
    }
    return <TextValue onClick={value.onClick}>{value.text}</TextValue>;
  }

  render() {
    const { label, value, ctaIcon, handleCtaClick } = this.props;

    return (
      <TextInfoWrapper>
        <TextInfoDetail>
          {label && <Label>{label}</Label>}
          <WrapperValues>{TextInfo.getValues(value)}</WrapperValues>
        </TextInfoDetail>
        {ctaIcon && (
          <TextInfoIcon onClick={handleCtaClick}>
            <Icon name={ctaIcon} />
          </TextInfoIcon>
        )}
      </TextInfoWrapper>
    );
  }
}

TextInfo.propTypes = {
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  value: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  ctaIcon: PropTypes.string,
  handleCtaClick: PropTypes.func,
};

export default TextInfo;
