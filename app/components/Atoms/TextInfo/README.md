# TextInfo

### Example
```js
import TextInfo from './components/TextInfo';

...
  return(
    <SectionWrapper>
      <BoxWrapper>
        <TextInfo
          label="Address"
          value={{ text: 'via Smerillo 20, Roma, Italy' }}
          ctaIcon="phone"
          handleCtaClick={handleCtaClick}
        />
      </BoxWrapper>
    </SectionWrapper>
  )


...

```

### Required props:
  - **label:** Accordion label (string/node)
  - **value:** Value text (object/array)
  - **ctaIcon:** Icon name (string)
  - **handleCtaClick:** Action on iconCTA click (func)

