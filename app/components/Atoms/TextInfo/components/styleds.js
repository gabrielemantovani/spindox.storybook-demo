import styled from 'styled-components';

export const TextInfoWrapper = styled.div`
  display: flex;
  align-items: center;
`;

export const Label = styled.div`
  display: block;
  color: ${(props) => props.theme.colors.secondaryText};
  font-family: ${(props) => props.theme.fonts.primaryFont};
  font-size: ${(props) => props.theme.sizes.smallText};
  padding-bottom: 8px;
`;

export const TextInfoDetail = styled.div`
  flex: 1 1 auto;
  
  a {
    text-decoration: none;
  }
`;

export const TextInfoIcon = styled.div`
  flex: 0 0 26px;
  
  i {
    color: ${(props) => props.theme.colors.highlightBorder};
  }
`;

export const WrapperValues = styled.div`
  display: flex;
`;

export const TextValue = styled.div`
  display: block;
  font-family: ${(props) => props.theme.fonts.primaryFont};
  color: ${(props) => props.theme.colors.primaryText};
  font-size: ${(props) => props.theme.sizes.text};
  padding-right: 28px;
`;
