export default (props) => `
  
  .Modal__Overlay {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: rgba(0,0,0,0.4);
    z-index: ${props.theme.constants.layers.indexOf('modals')}; 
    display: flex;
    justify-content: center;
    align-items: center;
  }
  
  .Modal__Content {
    position: relative;
    border: none;
    background: ${props.theme.colors.background};
    color: ${props.theme.colors.primaryText};
    overflow: auto;
    -webkit-overflow-scrolling: touch;
    border-radius: 0;
    outline: none;
  }
  
  /* fade animation */
  .Modal__Overlay--fade {
    opacity: 0;
    transition: opacity 300ms ease-in-out;    
  }
  
  .Modal__Overlay--afterOpen {
    opacity: 1;
  }
    
  .Modal__Overlay--beforeClose {
    opacity: 0;
  }
   
  /* slideUp animation */
  .Modal__Content--slideUp {
    transform: translateY(101vh);
    transition: transform 300ms ease-in-out; 
  }
  
  .Modal__Content--slideUp.Modal__Content--afterOpen {
    transform: translateY(0);
  }
  
  /* scale animation */
  .Modal__Content--scale {
    transform: scale(0.1);
    transition: transform 300ms ease-in-out;  
  }
  
  .Modal__Content--scale.Modal__Content--afterOpen {
    transform: scale(1);
  }
  
`;
