/**
 *
 * Modal
 *
 */

import React from 'react';
import { withTheme } from 'styled-components';
import PropTypes from 'prop-types';
import ReactModal from 'react-modal';

// components
import Icon from 'components/Atoms/Icon';

// styled
// import customStyles from './styles';
import { ButtonClose } from './components/styleds';

const getParent = () => document.querySelector('.MainLayout');
const baseContentClass = 'Modal__Content';
const baseOverlayClass = 'Modal__Overlay';

class Modal extends React.PureComponent {
  // eslint-disable-line react/prefer-stateless-function

  // constructor(props) {
  //   super(props);
  //
  //   // optimization for screen readers
  //   // ReactModal.setAppElement('#app');
  // }

  render() {
    const {
      children,
      showModal,
      handleCloseModal,
      animation,
      ...rest
    } = this.props;

    return (
      <ReactModal
        isOpen={showModal}
        parentSelector={getParent}
        className={{
          base: `${baseContentClass} ${baseContentClass}--${
            animation || 'fade'
          }`,
          afterOpen: 'Modal__Content--afterOpen',
          beforeClose: 'Modal__Content--beforeClose',
        }}
        overlayClassName={{
          base: `${baseOverlayClass} ${baseOverlayClass}--${animation}`,
          afterOpen: 'Modal__Overlay--afterOpen',
          beforeClose: 'Modal__Overlay--beforeClose',
        }}
        {...rest}
      >
        <ButtonClose onClick={handleCloseModal}>
          <Icon name="close" />
        </ButtonClose>
        {children && children}
      </ReactModal>
    );
  }
}

Modal.propTypes = {
  showModal: PropTypes.bool.isRequired,
  handleCloseModal: PropTypes.func,
  theme: PropTypes.object,
  animation: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.array.isRequired,
    PropTypes.object.isRequired,
  ]),
  parent: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.node,
  ]),
};

export default withTheme(Modal);
