import styled from 'styled-components';

export const ButtonClose = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  padding: 0 20px;
  z-index: ${(props) => props.theme.constants.layers.indexOf('modals') + 1};
  height: ${(props) => props.theme.sizes.modalHeaderHeight};
  display: flex;
  align-items: center;

  // temp
  i { font-size: 1rem; }
`;
