# Modal

This component it's based on [react-modal](https://github.com/reactjs/react-modal)

### Example
```js
import Modal from './components/Modal';

...
  return(
      <Modal showModal contentLabel="Basic Modal Example">
        <div style={{ width: '85vw', height: '400px'}}>
          <div style={{ paddingTop: '30px', textAlign: 'center'}}>
            Basic Modal
          </div>
        </div>
      </Modal>
  );


...

```

### Required props:
  - **showModal:** state of modal visibility (bool)
  - **handleCloseModal:** Function called on close button click (func)
  - **slideUp:** Option to enable the slideUp enter animation (bool)
  - **children:** Content of modal (node)

### Note

The Modal was append inside the the `.MainLayout`, so this element must be present on the view.

To make a `fullscreen` modal you must set this properties to the content: 

```
width: 100vw;
height: 100vh;
```

