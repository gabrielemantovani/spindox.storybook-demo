/*
 * DealershipModal Messages
 *
 * This contains all the text for the DealershipModal component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  consultantLabel: {
    id: 'app.components.DealershipModal.consultantLabel',
    defaultMessage: 'Sales consultant',
  },
  addressLabel: {
    id: 'app.components.DealershipModal.addressLabel',
    defaultMessage: 'Address',
  },
  websiteLabel: {
    id: 'app.components.DealershipModal.websiteLabel',
    defaultMessage: 'Website',
  },
  contactMe: {
    id: 'app.components.DealershipModal.contactMe',
    defaultMessage: 'Contact me',
  },
});
