/**
 *
 * InfoModal
 *
 */

import React from 'react';
import PropTypes from 'prop-types';

// import { FormattedMessage } from 'react-intl';
// import messages from './messages';

// components
import Modal from 'components/Organisms/Modal';

import AccordionContent from './components/AccordionContent';
import StandardContent from './components/StandardContent';

// styled
import { InfoModalWrapper } from './components/styleds';

const CONSULTANT_PROP = 'consultant';

class InfoModal extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  static getModalContent(dealership, props) {
    return Object.prototype.hasOwnProperty.call(dealership, CONSULTANT_PROP) ? (
      <AccordionContent {...{ dealership, ...props }} />
    ) : (
      <StandardContent {...{ dealership, ...props }} />
    );
  }

  render() {
    const { showModal, handleCloseModal, animation, dealership, ...rest } = this.props;
    return (
      <InfoModalWrapper>
        <Modal {...{ showModal, handleCloseModal, animation }}>
          {dealership && InfoModal.getModalContent(dealership, rest)}
        </Modal>
      </InfoModalWrapper>
    );
  }
}

InfoModal.propTypes = {
  showModal: PropTypes.bool.isRequired,
  dealership: PropTypes.object,
  handleCloseModal: PropTypes.func,
  animation: PropTypes.string,
  ctaLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  handleCtaClick: PropTypes.func,
  handleBookAppointment: PropTypes.func,
};

export default InfoModal;
