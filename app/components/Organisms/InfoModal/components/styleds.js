import styled from 'styled-components';

export const InfoModalWrapper = styled.div`
  position: relative;
  width: 100%;
`;

export const AccordionContentWrapper = styled.div`
  width: ${(props) => props.theme.sizes.modalBoxedWidth};
  height: auto;
  min-height: 300px; 
`;

export const StandardContentWrapper = styled.div`
  width: ${(props) => props.theme.sizes.modalBoxedWidth};
  height: auto;
  min-height: 300px;
`;

export const DealershipSection = styled.div`
  display: block;
  width: 100%;
  padding-top: 56px;
  background-color: ${(props) => props.theme.colors.background};
`;

export const DealerTitle = styled.div`
  display: block;
  font-family: ${(props) => props.theme.fonts.primaryFont};
  color: ${(props) => props.theme.colors.primaryText};
  font-size: ${(props) => props.theme.sizes.h1};
  padding-bottom: 28px;
`;

export const FavouriteIcon = styled.div`
  display: block;
  position: absolute;
  top: 0;
  right: 0;
  height: ${(props) => props.theme.sizes.modalHeaderHeight};
  padding: 0 20px;
  display: flex;
  align-items: center;
`;

export const ConsultantSection = styled.div`
  display: block;
  background-color: ${(props) => props.theme.colors.boxBackground};
  padding: 24px 0 12px 0;
  position: relative;
  
  .rah-static {
    overflow: visible !important;
  }
  
  .rah-static--height-zero > div {
    display: block !important;
  }
`;

export const DealerContact = styled.div`
  padding: 12px 0;
`;

export const ToggleButton = styled.div`
  width: 100%;
  text-align: center;
  padding-bottom: 12px;
  
  i {
    display: block;
    margin: 0 auto;
    color: ${(props) => props.theme.colors.highlightBorder};
    transform: rotate(${(props) => props.showMore ? '-180deg' : '0'});
    transition: transform 0.3s ease;
  }
`;

export const ConsultantLabel = styled.div`
  display: block;
  font-family: ${(props) => props.theme.fonts.primaryFont};
  color: ${(props) => props.theme.colors.secondaryText};
  font-size: ${(props) => props.theme.sizes.smallText};
  text-transform: uppercase;
  padding-bottom: 8px;
`;

export const ConsultantName = styled.div`
  display: block;
  font-family: ${(props) => props.theme.fonts.primaryFont};
  color: ${(props) => props.theme.colors.primaryText};
  font-size: ${(props) => props.theme.sizes.h1};
  padding-bottom: 24px;
`;

export const ConsultantContact = styled.div`
  padding: 12px 0;
`;

export const ConsultantPhone = styled.div`
  padding: 12px 0;
  
  i {
    display: block;
    transition: all 0.3s ease;
    ${(props) => props.showMore ? 'position: absolute; bottom: 44px;' : 'position: relative;'};
  }
`;

export const ActionButton = styled.div`
  display: block;
  width: 100%;
  padding-top: 12px;
`;

export const TopRightCta = styled.div`
  display: block;
  position: absolute;
  top: 0;
  right: 0;
  padding: 16px 20px;
  font-family: ${(props) => props.theme.fonts.primaryFont};
  color: ${(props) => props.theme.colors.highlightText};
  font-size: ${(props) => props.theme.sizes.text};
`;
