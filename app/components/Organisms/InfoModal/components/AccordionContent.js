import React from 'react';
import PropTypes from 'prop-types';
import AnimateHeight from 'react-animate-height';
import { FormattedMessage } from 'react-intl';

// components
import Icon from 'components/Atoms/Icon';
import BoxWrapper from 'components/Atoms/BoxWrapper';
import TextInfo from 'components/Atoms/TextInfo';
import Button from 'components/Atoms/Button';

// utils
import { navigate } from 'utils/geolocation';
import { openPhone, openMail, openExternalLink } from 'utils/system';

// styled
import {
  AccordionContentWrapper,
  DealershipSection,
  DealerTitle,
  FavouriteIcon,
  ConsultantSection,
  DealerContact,
  ToggleButton,
  ConsultantLabel,
  ConsultantName,
  ConsultantPhone,
  ConsultantContact,
  TopRightCta,
} from './styleds';

import messages from '../messages';

class AccordionContent extends React.PureComponent {
  static handleAddressClick(address) {
    if (address && address.length > 0) navigate(address);
  }

  static handlePhoneClick(number) {
    if (number && number.length > 0) openPhone(number);
  }

  static handleMailClick(mail) {
    if (mail && mail.length > 0) openMail(mail);
  }

  static handleWebsiteClick(url) {
    if (url && url.length > 0) openExternalLink(url);
  }

  constructor(props) {
    super(props);

    this.state = {
      showMore: false,
    };

    this.toggleMore = this.toggleMore.bind(this);
  }

  toggleMore() {
    this.setState((state) => {
      const { showMore } = state;
      return {
        showMore: !showMore,
      };
    });
  }

  render() {
    const { dealership, ctaLabel, handleCtaClick, handleBookAppointment, preferred, onStarClick } = this.props;
    const {
      title,
      address,
      phones,
      mail,
      consultant,
      website,
    } = dealership;
    const {
      name: consultantName,
      phones: consultantPhones,
      mail: consultantMail,
    } = consultant;
    const { showMore } = this.state;

    return (
      <AccordionContentWrapper>
        <DealershipSection>
          <BoxWrapper>
            <DealerTitle>{title && title}</DealerTitle>
          </BoxWrapper>
          {ctaLabel ? (
            <TopRightCta onClick={handleCtaClick}>{ctaLabel}</TopRightCta>
          ) : (
            <FavouriteIcon onClick={onStarClick}>
              <Icon name={preferred ? 'star-on' : 'star-off'} />
            </FavouriteIcon>
          )}
          <BoxWrapper>
            {address && (
              <DealerContact>
                <TextInfo
                  label={<FormattedMessage {...messages.addressLabel} />}
                  value={{
                    text: address,
                    onClick: () => AccordionContent.handleAddressClick(address),
                  }}
                  handleCtaClick={() =>
                    AccordionContent.handleAddressClick(address)
                  }
                  ctaIcon="geolocalization"
                />
              </DealerContact>
            )}
            <AnimateHeight duration={300} height={showMore ? 'auto' : 0}>
              {phones && (
                <DealerContact>
                  <TextInfo
                    value={phones.map((e) => ({
                      text: e,
                      onClick: () => AccordionContent.handlePhoneClick(e),
                    }))}
                    ctaIcon="phone"
                    handleCtaClick={() =>
                      AccordionContent.handlePhoneClick(phones[0])
                    }
                  />
                </DealerContact>
              )}
              {mail && (
                <DealerContact>
                  <TextInfo
                    value={{
                      text: mail,
                      onClick: () => AccordionContent.handleMailClick(mail),
                    }}
                    ctaIcon="mail"
                    handleCtaClick={() =>
                      AccordionContent.handleMailClick(mail)
                    }
                  />
                </DealerContact>
              )}
              {website && (
                <DealerContact>
                  <TextInfo
                    value={{
                      text: website,
                      onClick: AccordionContent.handleWebsiteClick(website),
                    }}
                    handleCtaClick={() =>
                      AccordionContent.handleWebsiteClick(website)
                    }
                    label={<FormattedMessage {...messages.websiteLabel} />}
                  />
                </DealerContact>
              )}
            </AnimateHeight>
          </BoxWrapper>
        </DealershipSection>
        <ToggleButton showMore={showMore}>
          <Icon name="chevron" onClick={this.toggleMore} />
        </ToggleButton>
        <ConsultantSection>
          <BoxWrapper>
            <ConsultantLabel>
              <FormattedMessage {...messages.consultantLabel} />
            </ConsultantLabel>
            <ConsultantName>{consultantName}</ConsultantName>
            <AnimateHeight duration={300} height={showMore ? 0 : 'auto'}>
              {consultantPhones && (
                <ConsultantPhone showMore={showMore}>
                  <TextInfo
                    value={consultantPhones.map((e) => ({
                      text: e,
                      onClick: () => AccordionContent.handlePhoneClick(e),
                    }))}
                    handleCtaClick={() =>
                      AccordionContent.handlePhoneClick(consultantPhones[0])
                    }
                    ctaIcon="phone"
                  />
                </ConsultantPhone>
              )}
              {consultantMail && (
                <ConsultantContact>
                  <TextInfo
                    value={{
                      text: consultantMail,
                      onClick: () =>
                        AccordionContent.handleMailClick(consultantMail),
                    }}
                    ctaIcon="mail"
                    handleCtaClick={() =>
                      AccordionContent.handleMailClick(consultantMail)
                    }
                  />
                </ConsultantContact>
              )}
            </AnimateHeight>
          </BoxWrapper>
        </ConsultantSection>
        <Button
          label={<FormattedMessage {...messages.contactMe} />}
          style={{ width: '100%' }}
          // icon="calendar-plus"
          onClick={handleBookAppointment}
        />
      </AccordionContentWrapper>
    );
  }
}

AccordionContent.propTypes = {
  dealership: PropTypes.object,
  ctaLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  handleCtaClick: PropTypes.func,
  handleBookAppointment: PropTypes.func,
  onStarClick: PropTypes.func,
  preferred: PropTypes.bool,
};

export default AccordionContent;
