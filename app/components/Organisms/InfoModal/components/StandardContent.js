import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

// components
import Icon from 'components/Atoms/Icon';
import BoxWrapper from 'components/Atoms/BoxWrapper';
import TextInfo from 'components/Atoms/TextInfo';
import Button from 'components/Atoms/Button';

// utils
import { navigate } from 'utils/geolocation';
import { openExternalLink, openMail, openPhone } from 'utils/system';

// styled
import {
  StandardContentWrapper,
  DealershipSection,
  DealerTitle,
  FavouriteIcon,
  DealerContact,
  ActionButton,
  TopRightCta,
} from './styleds';

import messages from '../messages';

class StandardContent extends React.PureComponent {
  static handleAddressClick(address) {
    if (address && address.length > 0) navigate(address);
  }

  static handlePhoneClick(number) {
    if (number && number.length > 0) openPhone(number);
  }

  static handleMailClick(mail) {
    if (mail && mail.length > 0) openMail(mail);
  }

  static handleWebsiteClick(url) {
    if (url && url.length > 0) openExternalLink(url);
  }

  render() {
    const { dealership, ctaLabel, handleCtaClick, handleBookAppointment, preferred, onStarClick } = this.props;
    const { title, address, phones, mail, website } = dealership;

    return (
      <StandardContentWrapper>
        <DealershipSection>
          <BoxWrapper>
            <DealerTitle>{title && title}</DealerTitle>
          </BoxWrapper>
          {ctaLabel ? (
            <TopRightCta onClick={handleCtaClick}>{ctaLabel}</TopRightCta>
          ) : (
            <FavouriteIcon onClick={onStarClick}>
              <Icon name={preferred ? 'star-on' : 'star-off'} />
            </FavouriteIcon>
          )}
          <BoxWrapper>
            {address && (
              <DealerContact>
                <TextInfo
                  label={<FormattedMessage {...messages.addressLabel} />}
                  value={{
                    text: address,
                    onClick: () => StandardContent.handleAddressClick(address),
                  }}
                  handleCtaClick={() =>
                    StandardContent.handleAddressClick(address)
                  }
                  ctaIcon="geolocalization"
                />
              </DealerContact>
            )}
            {phones && (
              <DealerContact>
                <TextInfo
                  value={phones.map((e) => ({
                    text: e,
                    onClick: () => StandardContent.handlePhoneClick(e),
                  }))}
                  handleCtaClick={() =>
                    StandardContent.handlePhoneClick(phones[0])
                  }
                  ctaIcon="phone"
                />
              </DealerContact>
            )}
            {mail && (
              <DealerContact>
                <TextInfo
                  value={{
                    text: mail,
                    onClick: () => StandardContent.handleMailClick(mail),
                  }}
                  ctaIcon="mail"
                  handleCtaClick={() => StandardContent.handleMailClick(mail)}
                />
              </DealerContact>
            )}
            {website && (
              <DealerContact>
                <TextInfo
                  value={{
                    text: website,
                    onClick: () => StandardContent.handleWebsiteClick(website),
                  }}
                  handleCtaClick={() =>
                    StandardContent.handleWebsiteClick(website)
                  }
                  label={<FormattedMessage {...messages.websiteLabel} />}
                />
              </DealerContact>
            )}
          </BoxWrapper>
        </DealershipSection>
        <ActionButton>
          <Button
            label={<FormattedMessage {...messages.contactMe} />}
            style={{ width: '100%' }}
            // icon="calendar-plus"
            onClick={handleBookAppointment}
          />
        </ActionButton>
      </StandardContentWrapper>
    );
  }
}

StandardContent.propTypes = {
  dealership: PropTypes.object,
  ctaLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  handleCtaClick: PropTypes.func,
  handleBookAppointment: PropTypes.func,
  onStarClick: PropTypes.func,
  preferred: PropTypes.bool,
};

export default StandardContent;
