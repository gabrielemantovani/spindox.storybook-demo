/**
 *
 * RadioToggle
 *
 */

import React from 'react';
import PropTypes from 'prop-types';

// components
import RadioButton from 'components/Atoms/RadioButton';

import { RadioToggleWrapper } from './components/styleds';

class RadioToggle extends React.PureComponent {
  // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    const { options, defaultSelected } = props;

    this.state = {
      options: options.map((option) =>
        Object.assign({}, option, { selected: option.id === defaultSelected })
      ),
    };

    this.updateOptions = this.updateOptions.bind(this);
  }

  updateOptions(option) {
    const { onChange } = this.props;
    this.setState(
      ({ options }) => ({
        options: options.map((e) =>
          Object.assign({}, e, { selected: e.id === option.id })
        ),
      }),
      () => onChange(this.state.options)
    );
  }

  render() {
    const { options } = this.state;
    const { disabled, className } = this.props;

    return (
      <RadioToggleWrapper {...{ className }}>
        {options &&
          options.map((option) => (
            <RadioButton
              key={option.id}
              value={option.selected}
              label={option.label}
              onClick={() => this.updateOptions(option)}
              disabled={disabled}
            />
          ))}
      </RadioToggleWrapper>
    );
  }
}

RadioToggle.propTypes = {
  options: PropTypes.array.isRequired,
  onChange: PropTypes.func,
  defaultSelected: PropTypes.number,
  disabled: PropTypes.bool,
  className: PropTypes.string,
};

export default RadioToggle;
