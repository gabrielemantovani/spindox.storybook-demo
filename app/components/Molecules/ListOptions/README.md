# ListOptions

### Example
```js
import ListOptions from './components/ListOptions';

const options = [
  {
     "label": "Option 1",
     "id": 1,
     "selected": false
   },
   {
     "label": "Option 2",
     "id": 2,
     "selected": true
   },
   {
     "label": "Option 3",
     "id": 3,
     "selected": false
   }
];
...
  return(
    <ListOptions options={options} type="radio" />
  )


...

```

### Required props:
  - **type:** The behavior of component: select / radio (string)
  - **options:** Array of options object (array)
  - **allOptions:** Render the all option element (bool)
  - **onChange:** Handler function with the new options state (func)
  - **entree:** Flag to activate an entree animation - cascade slide in - (bool)
