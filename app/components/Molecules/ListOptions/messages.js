/*
 * ListOptions Messages
 *
 * This contains all the text for the ListOptions component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  allLabel: {
    id: 'app.components.ListOptions.allLabel',
    defaultMessage: 'All',
  },
});
