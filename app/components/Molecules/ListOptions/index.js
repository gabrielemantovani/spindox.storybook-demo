/**
 *
 * ListOptions
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import pull from 'lodash/pull';
import { FormattedMessage } from 'react-intl';

// components
import BasicOption from './components/BasicOption';

// styled
import { ListOptionsWrapper } from './components/styleds';

import messages from './messages';

const AllOptionElement = {
  label: <FormattedMessage {...messages.allLabel} />,
  id: 0,
  selected: false,
};

class ListOptions extends React.PureComponent {
  // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);

    const { options, allOption, type } = props;
    const opts = (allOption) ? [AllOptionElement, ...options] : pull(options, (e) => e.id === 0);

    this.state = {
      options: opts,
      type,
    };

    this.handleToggle = this.handleToggle.bind(this);
  }

  toggleOption(id, options, type) {
    return options.map((e) => {
      if (type === 'select' && e.id === id) e.selected = !e.selected;
      if (type === 'radio') e.selected = e.id === id;
      return e;
    });
  }

  handleToggle(id) {
    const { onChange, type } = this.props;
    const { options } = this.state;
    const isAll = id === AllOptionElement.id;

    if (isAll) {
      options[0].selected = !options[0].selected;
      this.setState({
        options: options.map((e) => { e.selected = options[0].selected; return e; }),
      });
    } else {
      const { allOption } = this.props;
      if (allOption) options[0].selected = false;
      this.setState({
        options: this.toggleOption(id, options, type),
      });
    }

    onChange(options);
  }

  render() {
    const { options } = this.state;
    const { entree } = this.props;

    return (
      <ListOptionsWrapper q={options.length} entree={entree}>
        {options &&
          options.map((e) => (
            <BasicOption {...e} key={e.id} onClick={() => this.handleToggle(e.id)} />
          ))}
      </ListOptionsWrapper>
    );
  }
}

ListOptions.propTypes = {
  allOption: PropTypes.bool,
  type: PropTypes.string.isRequired,
  options: PropTypes.array.isRequired,
  onChange: PropTypes.func,
  entree: PropTypes.bool,
};

export default ListOptions;
