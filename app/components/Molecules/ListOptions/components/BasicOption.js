import React from 'react';
import PropTypes from 'prop-types';

import Icon from 'components/Atoms/Icon';

import { BasicOptionWrapper } from './styleds';

class BasicOption extends React.PureComponent {
  // eslint-disable-line react/prefer-stateless-function

  render() {
    const { label, selected, onClick, iconLeft } = this.props;
    return (
      <BasicOptionWrapper {...{ onClick, selected, iconLeft }} className={selected ? 'selected' : ''}>
        <div className="inner">
          {iconLeft && <span className="icon-left"><Icon name={iconLeft} /></span>}
          <span className="label">{label}</span>
          <span className="icon-right"><Icon name="tick" /></span>
        </div>
      </BasicOptionWrapper>
    );
  }
}

BasicOption.propTypes = {
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  selected: PropTypes.bool,
  onClick: PropTypes.func,
  iconLeft: PropTypes.string,
};

export default BasicOption;
