import styled, { keyframes } from 'styled-components';

// Constants
const TIME_FLIP_ANIMATION = '0.4s';

// Animations
const slideEntree = keyframes`
  from {
    transform: translateX(101vw);
    opacity: 0;
  }

  to {
    transform: translateX(0);
    opacity: 1;
  }  
`;

const getEntreeAnimation = (props) => {
  const { q } = props;
  let output = `
    & > div {
    animation: ${slideEntree} 0.4s ease-in;
    transform: translateX(101vw);
    opacity: 0;
    animation-fill-mode: forwards;
  }
  `;
  let i = 1;

  while (i <= q) {
    output += `& > div:nth-child(${i}) { animation-delay:${i * 0.2}s }`;
    i += 1;
  }
  return output;
};

// Components
export const ListOptionsWrapper = styled.div`
  display: block;
  width: 100%;
  box-sizing: border-box;
  overflow: hidden;

  ${(props) => (props.entree ? getEntreeAnimation(props) : '')};
`;

const ICON_LEFT_WIDTH = '24px';

export const BasicOptionWrapper = styled.div`
  display: block;
  width: 100%;
  position: relative;
  padding: 16px;
  box-sizing: border-box;
  margin: 12px 0;
  background: transparent;

  .inner {
    display: flex;
    align-items: center;
    position: relative;
    z-index: 1;
    padding-left: ${(props) => props.iconLeft ? '36px' : ''};
  }

  &.selected {
    &:before {
      transform: rotateX(180deg);
    }

    &:after {
      opacity: 1;
    }

    span.icon-right {
      opacity: 1;
    }
  }
  
  &:before,
  &:after {
    content: '';
    display: block;
    height: 100%;
    width: 100%;
    position: absolute;
    top: 0;
    left: 0; 
  }

  &:before {
    background: transparent;
    border: solid 1px ${(props) => props.theme.colors.selectBorder};

    perspective: 800px;
    transform-style: preserve-3d;
    transition: all ${TIME_FLIP_ANIMATION};
    box-sizing: border-box;
    transform: rotateX(0deg);

    backface-visibility: hidden;
    -moz-backface-visibility: hidden;
    -webkit-backface-visibility: hidden;
  }

  &:after {
    background-color: ${(props) => props.theme.colors.boxBackground};
    transition: opacity ${TIME_FLIP_ANIMATION};
    opacity: 0;
    z-index: 0;
  }

  span.label {
    color: ${(props) => props.theme.colors.primaryText};
    font-family: ${(props) => props.theme.fonts.primaryFont};
    font-size: ${(props) => props.theme.sizes.text};
    position: relative;
  }

  span.icon-right {
    position: absolute;
    right: 4px;
    top: 50%;
    transform: translateY(-50%);
    opacity: 0;
    transition: all 0.2s;
    transition-delay: 0.2s;
    line-height: 1;

    // temp for mock icon
    i {
      font-size: 18px;
    }
  }
  
  .icon-left {
  
    position: absolute;
    left: 4px;
    top: 50%;
    transform: translateY(-50%);
    line-height: 1;
    width: ${ICON_LEFT_WIDTH};

  }
`;
