const sizes = {
  common: {
    headerHeight: '8vh',
    modalHeaderHeight: '56px',
    paddingFrame: '24px',
    buttonHeight: '50px',
    tabsTrayHeight: '10vh',
    tabsHeaderHeight: '8vh',
    icon: '1.5rem',
    headerIcon: 1.2,
    modalBoxedWidth: '85vw',
    bigTitle: '1.875rem',       // 30px
    h1: '1.375rem',             // 22px
    h2: '1.25rem',              // 20px
    h3: '1.125rem',             // 18px
    bigText: '1rem',            // 16px
    text: '0.875rem',           // 14px
    smallText: '0.75rem',       // 12px
    smallestText: '0.625rem',   // 10px
  },
};

export default (device = 'common') => sizes[device];
