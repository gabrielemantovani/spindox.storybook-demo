const constants = {
  common: {
    layers: ['header', 'tabsContent', 'ButtonFixed', 'tabsTray', 'modals'],
  },
};

export default (device = 'common') => constants[device];
