/**
 * Created by Stefano Demurtas on 25/10/2017.
 */
import dark from './dark';

export default {
  dark,
  // ... other theme
};
