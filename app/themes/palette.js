/*
* Design System
* Source: Invision
* Designer: Ilaria Farina
*
* */

// + COLOR PALETTE
export const RED = '#EB2E2C';
export const WHITE = '#FFFFFF';
export const WHITE_OPACITY = 'rgba(255, 255, 255, 0.5)';
export const WHITE_OPACITY_7 = 'rgba(255, 255, 255, 0.7)';
export const MIDDLE_YELLOW = '#FFE700';
export const CHARLESTON_GREEN = '#272A2F';
export const RAISIN_BLACK = '#212226';
export const BLACK = '#000000';
export const RAISIN_BLACK_2 = '#222222';
export const LIGHT_GREEN = '#83E18C';
export const MUSTARD = '#FFD750';

// + GRAY SCALE
export const TROLLEY_GREY = '#808080';
export const GAINSBORO = '#DEDEDE';
export const ANTIFLASH_WHITE = '#F2F2F2';
export const DARK_LIVER = '#4D4D4D';
export const ISABELLINE = '#EAEAEA';
export const DARK_MEDIUM_GRAY = '#AAAAAA';
