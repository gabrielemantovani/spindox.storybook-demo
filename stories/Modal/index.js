/**
 *
 * Modal storybook
 *
 */

import React from 'react';
import { storiesOf } from '@storybook/react';
import withReadme from 'storybook-readme/with-readme';
// import { FormattedMessage } from 'react-intl';

import { MainLayoutWrapper } from '../../app/containers/MainLayout/components/styleds';

import Modal from '../../app/components/Organisms/Modal';
import ModalDocs from '../../app/components/Organisms/Modal/README.md';

const MainLayoutDecorator = (story) => (
  <MainLayoutWrapper className="MainLayout">{story()}</MainLayoutWrapper>
);

storiesOf('Organisms/Modal', module)
  .addDecorator(MainLayoutDecorator)
  .add(
    'basic',
    withReadme([ModalDocs], () => (
      <Modal showModal contentLabel="Basic Modal Example">
        <div style={{ width: '85vw', height: '400px' }}>
          <div style={{ paddingTop: '30px', textAlign: 'center' }}>
            Basic Modal
          </div>
        </div>
      </Modal>
    ))
  )
  .add(
    'full screen',
    withReadme([ModalDocs], () => (
      <Modal showModal contentLabel="Basic Modal Example">
        <div style={{ width: '100vw', height: '100vh' }}>
          <div style={{ paddingTop: '30px', textAlign: 'center' }}>
            Basic Modal
          </div>
        </div>
      </Modal>
    ))
  )
  .add(
    'full screen + slideUp',
    withReadme([ModalDocs], () => (
      <Modal showModal contentLabel="Basic Modal Example" slideUp>
        <div style={{ width: '100vw', height: '100vh' }}>
          <div style={{ paddingTop: '30px', textAlign: 'center' }}>
            Basic Modal
          </div>
        </div>
      </Modal>
    ))
  );
