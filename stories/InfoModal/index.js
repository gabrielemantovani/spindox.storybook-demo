/**
*
* InfoModal storybook
*
*/

import React from 'react';

import { storiesOf } from '@storybook/react';
import withReadme from 'storybook-readme/with-readme';
// import { FormattedMessage } from 'react-intl';


import InfoModal from '../../app/components/Organisms/InfoModal';
import InfoModalDocs from '../../app/components/Organisms/InfoModal/README.md';
import { MainLayoutWrapper } from '../../app/containers/MainLayout/components/styleds';

// stubs
import data from '../../app/components/Organisms/InfoModal/stubs/data.json';

const dataWithConsultant = data[0];
const dataBasic = data[1];

const MainLayoutDecorator = (story) => (
  <MainLayoutWrapper className="MainLayout">{story()}</MainLayoutWrapper>
);

storiesOf('Organisms/InfoModal', module)
  .addDecorator(MainLayoutDecorator)
  .add('basic', withReadme([InfoModalDocs], () => <InfoModal dealership={dataBasic} showModal />))
  .add('with cta', withReadme([InfoModalDocs], () => <InfoModal dealership={dataBasic} showModal ctaLabel="Change" />))
  .add('with consultant', withReadme([InfoModalDocs], () => <InfoModal dealership={dataWithConsultant} showModal />));
