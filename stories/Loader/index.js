/**
*
* Loader storybook
*
*/

import React from 'react';

import { storiesOf } from '@storybook/react';
import withReadme from 'storybook-readme/with-readme';
import centered from '../../.storybook/decorators/centered';

import Loader from '../../app/components/Atoms/Loader';
import LoaderDocs from '../../app/components/Atoms/Loader/README.md';

storiesOf('Atoms/Loader', module)
  .addDecorator(centered)
  .add('basic', withReadme([LoaderDocs], () => <Loader />));
