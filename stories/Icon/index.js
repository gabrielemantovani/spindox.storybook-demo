import React from 'react';

import { storiesOf } from '@storybook/react';
import withReadme from 'storybook-readme/with-readme';
import centered from '../../.storybook/decorators/centered';


import Icon from '../../app/components/Atoms/Icon';
import IconDocs from '../../app/components/Atoms/Icon/README.md';

storiesOf('Atoms/Icon', module)
  .addDecorator(centered)
  .add(
    'default props',
    withReadme([IconDocs], () => (
      <Icon name="star-on" />
    ))
  )
  .add(
    'custom props',
    withReadme([IconDocs], () => (
      <Icon name="star-off" size={2} color="red" />
    ))
  );
