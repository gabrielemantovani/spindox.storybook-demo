/**
 *
 * RadioButton storybook
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { storiesOf } from '@storybook/react';
import withReadme from 'storybook-readme/with-readme';
import centered from '../../.storybook/decorators/centered';

import RadioButton from '../../app/components/Atoms/RadioButton';
import RadioButtonDocs from '../../app/components/Atoms/RadioButton/README.md';

class MockContainer extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      value: false,
    };
  }

  render() {
    return (
      <RadioButton
        value={this.state.value}
        onClick={() => this.setState(({ value }) => ({ value: !value }))}
        disabled={this.props.disabled}
        label={this.props.label}
      />
    );
  }
}

MockContainer.propTypes = {
  disabled: PropTypes.bool,
  label: PropTypes.string,
};

storiesOf('Atoms/RadioButton', module)
  .addDecorator(centered)
  .add('basic', withReadme([RadioButtonDocs], () => <MockContainer />))
  .add(
    'disabled',
    withReadme([RadioButtonDocs], () => <MockContainer disabled label="Yes" />)
  )
  .add(
    'with label',
    withReadme([RadioButtonDocs], () => <MockContainer label="Yes" />)
  );
