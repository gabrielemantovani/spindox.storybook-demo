/**
 *
 * RadioToggle storybook
 *
 */

import React from 'react';

import { storiesOf } from '@storybook/react';
import withReadme from 'storybook-readme/with-readme';
import centeredFullBoxed from '../../.storybook/decorators/centeredFullBoxed';

import RadioToggle from '../../app/components/Molecules/RadioToggle';
import RadioToggleDocs from '../../app/components/Molecules/RadioToggle/README.md';

const options = [
  {
    id: 0,
    label: 'Yes',
  },
  {
    id: 1,
    label: 'No',
  },
];

storiesOf('Molecules/RadioToggle', module)
  .addDecorator(centeredFullBoxed)
  .add(
    'basic',
    withReadme([RadioToggleDocs], () => (
      <RadioToggle
        options={options}
        defaultSelected={1}
        onChange={(option) => console.log(option)}
      />
    ))
  );
