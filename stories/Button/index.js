import React from 'react';

import { storiesOf } from '@storybook/react';
import withReadme from 'storybook-readme/with-readme';
import centered from '../../.storybook/decorators/centered';

import Button from '../../app/components/Atoms/Button';
import ButtonDocs from '../../app/components/Atoms/Button/README.md';

storiesOf('Atoms/Button', module)
  .addDecorator(centered)
  .add('primary', withReadme([ButtonDocs], () => <Button label="ADD NEW ITEM" type="primary" />))
  .add(
    'primary disabled',
    withReadme([ButtonDocs], () => <Button label="ADD NEW ITEM" disabled />)
  )
  .add('primary with icon', withReadme([ButtonDocs], () => <Button label="ADD NEW ITEM" type="primary" icon="cart" />))
  .add('secondary', withReadme([ButtonDocs], () => <Button label="ADD NEW ITEM" type="secondary" />))
  .add('secondary disabled', withReadme([ButtonDocs], () => <Button label="ADD NEW ITEM" type="secondary" disabled />))
  .add('secondary with icon', withReadme([ButtonDocs], () => <Button label="Take a picture" type="secondary" icon="photo" />))
  .add('primaryGhost', withReadme([ButtonDocs], () => <Button label="ADD NEW ITEM" type="primaryGhost" />))
  .add('secondaryGhost', withReadme([ButtonDocs], () => <Button label="ADD NEW ITEM" type="secondaryGhost" />))
  .add('without ripple', withReadme([ButtonDocs], () => <Button label="ADD NEW ITEM" type="primary" disableRipple />));

