import React from 'react';
import PropTypes from 'prop-types';
import { storiesOf } from '@storybook/react';
import withReadme from 'storybook-readme/with-readme';
import isEmail from 'validator/lib/isEmail';
import centeredFullBoxed from '../../.storybook/decorators/centeredFullBoxed';

import TextInput from '../../app/components/Atoms/TextInput';
import TextInputDocs from '../../app/components/Atoms/TextInput/README.md';

class TextInputWrapper extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      value: props.value || '',
    };

    this.onChange = this.onChange.bind(this);
  }

  onChange(event) {
    const target = event.target;
    const { value } = target;

    this.setState({
      value,
    });
  }

  render() {
    return (
      <div>
        <TextInput {...this.props} value={this.state.value} onChange={this.onChange} />
      </div>
    );
  }
}

TextInputWrapper.propTypes = {
  value: PropTypes.string,
};

storiesOf('Atoms/TextInput', module)
  .addDecorator(centeredFullBoxed)
  .add(
    'basic',
    withReadme([TextInputDocs], () => (
      <TextInputWrapper id="email" type="text" label="Name" />
    ))
  )
  .add(
    'filled',
    withReadme([TextInputDocs], () => (
      <TextInputWrapper
        id="email"
        type="email"
        label="Email"
        value="mario.rossi@gmail.com"
      />
    ))
  )
  .add(
    'read only',
    withReadme([TextInputDocs], () => (
      <TextInputWrapper
        id="email"
        type="email"
        label="Email"
        disabled
        value="mario.rossi@gmail.com"
      />
    ))
  )
  .add(
    'with validation',
    withReadme([TextInputDocs], () => (
      <TextInputWrapper
        id="email"
        type="email"
        label="Email"
        validate={(value) => isEmail(value)}
        value=""
      />
    ))
  );
