/* eslint-disable */
import React from 'react';

import { storiesOf } from '@storybook/react';
import withReadme from 'storybook-readme/with-readme';
// import { FormattedMessage } from 'react-intl';

import ListOptions from '../../app/components/Molecules/ListOptions';
import ListOptionsDocs from '../../app/components/Molecules/ListOptions/README.md';

// components
import BoxWrapper from '../../app/components/Atoms/BoxWrapper';

// stubs
import data from '../../app/components/Molecules/ListOptions/stubs/data.json';

const onChange = (opts) => console.log('Options changed:', opts);

storiesOf('Molecules/ListOptions', module)
  .add(
    'type radio',
    withReadme([ListOptionsDocs], () => (
      <BoxWrapper>
        <ListOptions
          options={data.options}
          onChange={onChange}
          type="radio"
        />
      </BoxWrapper>
    ))
  )
  .add(
    'type select',
    withReadme([ListOptionsDocs], () => (
      <BoxWrapper>
        <ListOptions
          options={data.options}
          onChange={onChange}
          type="select"
        />
      </BoxWrapper>
    ))
  )
  .add(
    'type select with all',
    withReadme([ListOptionsDocs], () => (
      <BoxWrapper>
        <ListOptions
          allOption
          options={data.options}
          onChange={onChange}
          type="select"
        />
      </BoxWrapper>
    ))
  )
  .add(
    'type select with entree animation',
    withReadme([ListOptionsDocs], () => (
      <BoxWrapper>
        <ListOptions
          allOption
          entree
          options={data.options}
          onChange={onChange}
          type="select"
        />
      </BoxWrapper>
    ))
  );
