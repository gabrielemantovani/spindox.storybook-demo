import { addDecorator, configure } from '@storybook/react';
import { setIntlConfig, withIntl } from 'storybook-addon-intl';
import { setOptions } from '@storybook/addon-options';
import React from 'react';

// from local path
import '../app/assets/fonts/Lato/lato.css';
import '../app/assets/icons/font/ccp-icons.css';
import './styles.css';

import themes from '../app/themes/index';
import { ThemeProvider } from 'styled-components';
import sizes from '../app/themes/sizes';
import constants from '../app/themes/constants';


const theme = {
    ...themes.dark,
    sizes: sizes(),
    constants: constants(),
};

// Load the locale data for all your defined locales
import { addLocaleData } from 'react-intl';
import enLocaleData from 'react-intl/locale-data/en';
import deLocaleData from 'react-intl/locale-data/de';

addLocaleData(enLocaleData);
addLocaleData(deLocaleData);

// Provide your messages
const messages = {
    en: { 'dealership.title': 'Dealership' }
    // 'de': { 'button.label': 'Klick mich!' }
};

const getMessages = locale => messages[locale];

// Set intl configuration
setIntlConfig({
    locales: ['en', 'it'],
    defaultLocale: 'en',
    getMessages
});

setOptions({
    /**
     * name to display in the top left corner
     * @type {String}
     */
    name: 'ATOMIC UI STORYBOOK',
    /**
     * URL for name in top left corner to link to
     * @type {String}
     */
    url: '#',
    /**
     * show story component as full screen
     * @type {Boolean}
     */
    goFullScreen: false,
    /**
     * display left panel that shows a list of stories
     * @type {Boolean}
     */
    showLeftPanel: true,
    /**
     * display horizontal panel that displays addon configurations
     * @type {Boolean}
     */
    showDownPanel: true,
    /**
     * display floating search box to search through stories
     * @type {Boolean}
     */
    showSearchBox: false,
    /**
     * show horizontal addons panel as a vertical panel on the right
     * @type {Boolean}
     */
    downPanelInRight: true,
    /**
     * sorts stories
     * @type {Boolean}
     */
    sortStoriesByKind: true,
    /**
     * regex for finding the hierarchy separator
     * @example:
     *   null - turn off hierarchy
     *   /\// - split by `/`
     *   /\./ - split by `.`
     *   /\/|\./ - split by `/` or `.`
     * @type {Regex}
     */
    hierarchySeparator: `/`,

    /**
     * sidebar tree animations
     * @type {Boolean}
     */
    sidebarAnimations: true,

    /**
     * id to select an addon panel
     * @type {String}
     */
    selectedAddonPanel: undefined, // The order of addons in the "Addons Panel" is the same as you import them in 'addons.js'. The first panel will be opened by default as you run Storybook
});

// Register decorator
addDecorator(withIntl);

addDecorator(story => (
    <ThemeProvider theme={theme}>{story()}</ThemeProvider>
));

function loadStories() {
    require('../stories');
}

configure(loadStories, module);
